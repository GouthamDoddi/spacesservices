const pool = require('../config/db');


const getAllRows = async (tableName, fromDate, toDate) => {
    const query = {
        name: `Get all ${tableName}`,
        text: `SELECT * from "public".${tableName} where created_at >= '${fromDate}' and created_at < '${toDate}'`,
    };
    console.log(query)

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getAllRowsWithCondition = async (tableName, column, value) => {
    console.log(tableName, column, value)
    const query = {
        name: `Get all ${tableName} with condition`,
        text: `SELECT * from "public".${tableName} WHERE ${column}=$1`,
        values: [ value ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getAllRowsWith2Conditions = async (tableName, column, column2, values) => {
    console.log(tableName, column, column2, values)

    const query = {
        name: `Get all ${tableName} with 2 conditions`,
        text: `SELECT * from "public".${tableName} WHERE ${column} = $1
        AND WHERE ${column2} = $2`,
        values: [ values ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const selectRow = async (tableName, id) => {
    const query = {
        name: `Select from ${tableName}`,
        text: `SELECT * from "public".${tableName} WHERE id=$1`,
        values: [ id ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};


module.exports = { getAllRows, getAllRowsWithCondition, getAllRowsWith2Conditions }