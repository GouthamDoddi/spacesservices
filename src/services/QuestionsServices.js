'use strict';

const pool = require('../config/db');

const getQuetionsByComplianceProjectType = async type => {
    const query = {
        name: 'get all questions matching with project type',
        text: 'SELECT * from "public".rev_questions WHERE compliance_project_type=$1',
        values: [ type ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

module.exports = { getQuetionsByComplianceProjectType };
