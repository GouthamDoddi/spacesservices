const pool = require('../config/db');

const getAllComplianceProjectRecordsCount =  async () => {
    const totalQuery = {
        name: `get All ComplianceProject Records Count`,
        text: `SELECT  COUNT(rev_compliance_projects.id) AS counter,type_id from public.rev_compliance_projects GROUP BY rev_compliance_projects.type_id`,
    };

    const completedQuery = {
        name: `get All completedComplianceProject Records Count`,
        text: `SELECT  type_id, COUNT(type_id) from public.rev_compliance_projects WHERE start_date is not null AND rev_compliance_projects.end_date is not null GROUP BY rev_compliance_projects.type_id`,
    };

    const inprogressQuery = {
        name: `get All in progress ComplianceProject Records Count`,
        text: `SELECT  type_id, COUNT(type_id) from public.rev_compliance_projects WHERE start_date is not null AND rev_compliance_projects.end_date is null GROUP BY rev_compliance_projects.type_id`,
    };

    const notStartedQuery = {
        name: `get All not started ComplianceProject Records Count`,
        text: `SELECT  type_id, COUNT(type_id) from public.rev_compliance_projects WHERE start_date is null AND rev_compliance_projects.end_date is null GROUP BY rev_compliance_projects.type_id`,
    };

    try {
        const totalCount =  await pool.query(totalQuery);
        const completedCount = await pool.query(completedQuery);
        const inProgressCount = await pool.query(inprogressQuery);
        const notStartedCount = await pool.query(notStartedQuery);
    console.log({
    totalCount,
    completedCount,
    inProgressCount,
    notStartedCount,
})
        return {
            totalCount: totalCount.rows,
            completedCount: completedCount.rows,
            inProgressCount: inProgressCount.rows,
            notStartedCount: notStartedCount.rows,
        }
    } catch (error) {
        console.error(error);

        return error;
    }
}

const complianceRecordsAnalisis =  async (fromDate, toDate) => {
    const query = {
        name: ' records analysis',
        text: `select  COUNT(id) AS counter,mandate,status from public.rev_compliance_records where created_at >= '${fromDate}' and created_at < '${toDate}'  GROUP BY mandate, status`,
    };


    try {
        return await pool.query(query);    
    } catch (error) {
        console.log(error);

        return error;
    }
}


module.exports = { getAllComplianceProjectRecordsCount, complianceRecordsAnalisis };