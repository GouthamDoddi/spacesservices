'use strict';

const pool = require('../config/db');


const attributecount = async (id, fromDate, toDate) => {
    const query = {
        name: `Get all compliance projects related to a project`,
        text: `select count(distinct attribute_id) as attribute_id, count(distinct parameter_id) as paramater_id, CONCAT('12') as policies, CONCAT('13') as domains from public.rev_compliance_records where compliance_project_id = ${id} and created_at >= '${fromDate}' and created_at < '${toDate}'`,

    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
}

const complianceissues = async (id, fromDate, toDate) => {
    const query = {
        name: `Get all compliance projects related to a project`,
        text: `SELECT * from "public".rev_compliance_records WHERE compliance_project_id = ${id}  and result = 2 or result =3 and created_at >= '${fromDate}' and created_at < '${toDate}'`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
}


const complianceIssuesAnalisisWithProjectID = async (id, fromDate, toDate) => {
    // console.log(id, fromDate, toDate);
    const query = {
        name: `Select compliance records mandate`,
        text: `select  COUNT(c.id) AS counter,mandate,coalesce(status,0) from public.rev_compliance_records c where c.compliance_project_id= ${id} and created_at >= '${fromDate}' and created_at < '${toDate}' GROUP BY c.mandate,c.status`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}

const eservicelist = async (id, fromDate, toDate) => {
    // console.log(id);
    const query = {
        name: 'Select compliance e-service, changes, issues and casess',
        text: `select  count(c.id) as challenge_count,actual_type, concat('challenge') as challenge from public.challenges as c where c.project_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by c.actual_type
        UNION
        select  COUNT(id),type_id, concat('e-services') as challenge  from public.rev_compliance_projects where type_id=3 and id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by type_id
        UNION
        select count(id),compliance_project_id, concat('cases') as challenge from public.rev_cases  where compliance_project_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by compliance_project_id`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const eservicetablelist = async (id) => {
    const percentageQuery = {
        name: 'Select compliance e-service List Table, Percentage',
        text: `SELECT  COUNT(*) as total_counter, COUNT(*) FILTER (WHERE result=1) AS fully_compline_counter,  COUNT(*) FILTER (WHERE result=2) AS partially_counter
        FROM public.rev_compliance_records where compliance_project_id=${id}`
    };

    const mQuery = {
        name: 'Select compliance e-service List Table, M1, M2, M3',
        text: `select  COUNT(c.id) AS counter,mandate from public.rev_compliance_records c where c.compliance_project_id=${id} GROUP BY c.mandate`
    };

    const testedQuery = {
        name: 'Select compliance e-service List Table, tested',
        text: `select  COUNT(c.result) AS counter from public.rev_compliance_records c where c.compliance_project_id=${id} and result  IS not NULL`
    };

    const casesQuery = {
        name: 'Select compliance e-service List Table, cases',
        text: `select COUNT(*) from public.rev_cases where project_id=${id}`
    };

    const calScore = rows  => parseInt(((rows.fully_compline_counter + (rows.partially_counter*0.5))/rows.total_counter) * 100)

    try {
        const score =  await pool.query(percentageQuery);
        const percentage = calScore(score.rows[0])
        const issues =  await pool.query(mQuery);
        const tested =  await pool.query(testedQuery);
        const cases =  await pool.query(casesQuery);

        return {
            percentage: percentage,
            issues : issues.rows,
            tested : tested.rows,
            cases : cases.rows,
        }
    } catch (error) {
        console.log(error);
        return error;
    }
}




module.exports = { attributecount, complianceissues, complianceIssuesAnalisisWithProjectID, eservicelist, eservicetablelist }