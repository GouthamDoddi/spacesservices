const pool = require('../config/db');
const { getAllRows } = require('../services/servicesTemplate')

const insertEntity = async details => {
    const query = {
        name: 'insert new project',
        text: `INSERT INTO "public".rev_projects(
            project_name, project_name_ar, project_description,
            project_description_ar, sponsor_id, owner_id,
            project_type_id, logo_code, created_by, project_short_name
        ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING *`,
        values: [
            details.name,
            details.nameAr,
            details.description,
            details.descriptionAr,
            details.sponsor,
            details.owner,
            details.projectType,
            details.logo,
            details.createdBy,
            details.projectShortName,
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const getEntityScore = async (id, fromDate, toDate ) => {
    const query = {
        name: `get score for entity for ${id}`,
        text: `SELECT  COUNT(*) as total_counter, COUNT(*) FILTER (WHERE result=1) AS fully_compline_counter,  COUNT(*) FILTER (WHERE result=2) AS partially_counter
        FROM public.rev_compliance_records where owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}'`
    }

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
}


module.exports = { getEntityScore }