const pool = require('../config/db');


const getCasesCount = async (fromDate, toDate) => {

    const query = {
        name: 'Select compliance Projects',
        text: `SELECT count(id), status FROM public.rev_cases where created_at >= '${fromDate}' and created_at < '${toDate} group by rev_cases.status`,
    };

    try {
        
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

module.exports = { getCasesCount }