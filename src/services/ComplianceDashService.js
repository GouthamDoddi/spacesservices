'use strict';

const pool = require('../config/db');


// all project related services will be here


const getRevComplianceProject = async (id, fromDate, toDate) => {
    const query = {
        name: 'Select compliance Projects',
        text: `SELECT * from "public".rev_compliance_projects WHERE owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}'`
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const getComplianceRecordsStatus = async (id, fromDate, toDate) => {
    const query = {
        name: 'Select compliance records Status',
        text: `select  COUNT(id) AS counter,coalesce(status,0) from public.rev_compliance_records where compliance_project_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by status`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const getComplianceRecordsMandate = async (id, fromDate, toDate) => {
    const query = {
        name: 'Select compliance records mandate',
        text: `select  COUNT(c.id) AS counter,mandate from public.rev_compliance_records c where c.compliance_project_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' GROUP BY c.mandate`
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};



const getComplianceIssuesAnalysis = async (id, fromDate, toDate) => {

    console.log(id, fromDate, toDate);
    const query = {
        name: 'Select compliance records mandate',
        text: `select  COUNT(c.id) AS counter,mandate,coalesce(status,0) as status from public.rev_compliance_records c where c.compliance_project_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}'  GROUP BY c.mandate,c.status`
    };

    console.log(query);

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const getComplianceSection3 = async (id, fromDate, toDate) => {

    console.log(id);
    const query = {
        name: 'Select compliance e-service, changes, issues and casess',
        text: `select  count(c.id) as challenge_count,actual_type, concat('challenge') as challenge from public.challenges as c where c.entity_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by c.actual_type
        UNION
        select  COUNT(id),type_id, concat('e-services') as challenge  from public.rev_compliance_projects where type_id=3 and owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by type_id
        UNION
        select count(id),entity_id, concat('cases') as challenge from public.rev_cases  where entity_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by entity_id`,
    };
    console.log(query);
    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const getExceptionCases = async (id, fromDate, toDate) => {

    console.log(id, fromDate, toDate);
    const query = {
        name: 'Select compliance e-service, changes, issues and casess',
        text: `select  count(id),status  from public.rev_cases where entity_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by status`
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};





const getRegisteredStatusCountApps = async (id, fromDate, toDate) => {

    console.log(id, fromDate, toDate);

    const query = {
        name: 'Select Registered Total Apps',
        text: `select count(*) total, 'total' as status from  project_details WHERE owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}'  union 
        select count(*) total, 'inprogress' as status from  project_details WHERE owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' AND finalstatus='I'  union 
        select count(*) total, 'notstarted' as status from  project_details WHERE owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' AND finalstatus='N'  union 
        select count(*) total, 'completed' as status from  project_details WHERE owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' AND finalstatus='C' 
        `,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};


const getCompletedComplianceProjectAnalysis = async (id, fromDate, toDate) => {

    
    
    console.log(id, fromDate, toDate);

    const query = {
        name: `Select Registered Total Apps fro project ${id}`,
        text: `select t.type_id, t.total , coalesce(i.incount,0) inprogress,  coalesce(n. notstarted,0) notstarted,
        coalesce(c.completed,0) completed from
        ((select type_id ,count(*) total from  project_details WHERE owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}'  group by type_id ) t left join 
        (select type_id ,count(*) incount  from  project_details where finalstatus='I' and owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by type_id ) i on t.type_id = i.type_Id  left join
        (select type_id ,count(*) notstarted from  project_details where finalstatus='N' and owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by type_id ) n on t.type_id = n.type_Id left join
        (select type_id ,count(*) completed from  project_details where finalstatus='C' and owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by type_id ) c on t.type_id = c.type_Id)order by type_id`,
        
    };

    // console.log(query);

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const getCompletedComplianceProjectAnalysisForEntities = async (id, fromDate, toDate) => {

    
    
    console.log(id, fromDate, toDate);

    const query = {
        name: `Select Registered Total Apps fro project ${id}`,
        text: `select t.total , coalesce(i.incount,0) inprogress,  coalesce(n. notstarted,0) notstarted,
        coalesce(c.completed,0) completed from
        ((select type_id ,count(*) total from  project_details WHERE owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}'  group by type_id ) t left join 
        (select type_id ,count(*) incount  from  project_details where finalstatus='I' and owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by type_id ) i on t.type_id = i.type_Id  left join
        (select type_id ,count(*) notstarted from  project_details where finalstatus='N' and owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by type_id ) n on t.type_id = n.type_Id left join
        (select type_id ,count(*) completed from  project_details where finalstatus='C' and owner_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}' group by type_id ) c on t.type_id = c.type_Id)`,
        
    };

    // console.log(query);

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};


const eservice_cards_service = async (id, fromDate, toDate) => {
    console.log(id, fromDate, toDate);

    const query = {
        name: `Select eservices fro project ${id}`,
        text: `select * from public.rev_compliance_projects where type_id=3 and project_ids && '{${id}}' and created_at >= '${fromDate}' and created_at < '${toDate}'`,        
    };
    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);
        return error;
    }
};


module.exports = { 
    getRevComplianceProject,
    getComplianceRecordsStatus,
    getComplianceRecordsMandate,
    getComplianceIssuesAnalysis,
    getComplianceSection3,
    getExceptionCases,
    getRegisteredStatusCountApps,
    getCompletedComplianceProjectAnalysis,
    getCompletedComplianceProjectAnalysisForEntities,
    eservice_cards_service
};

