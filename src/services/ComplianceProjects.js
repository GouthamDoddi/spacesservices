'use strict';

const pool = require('../config/db');


// all project related services will be here

const insertComplianceProject = async details => {
    const query = {
        name: 'insert new project',
        text: `INSERT INTO "public".rev_compliance_projects(
            type, project_id, project_name, project_name_ar,
            project_description, project_description_ar, project_spoc,
            consumer_type, start_date, end_date, notes, created_by
        ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) RETURNING *`,
        values: [
            details.type,
            details.projectId,
            details.name,
            details.nameAr,
            details.description,
            details.descriptionAr,
            details.spoc,
            details.consumerType,
            details.startDate,
            details.endDate,
            details.notes,
            details.createdBy,
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const getComplianceProjectByEntityId = async id => {
    const query = {
        name: `Get all compliance projects related to a project`,
        text: `SELECT * from "public".rev_compliance_projects WHERE owner_id=${id}`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getCompletedComplianceProjectByEntityId = async id => {
    const query = {
        name: `Get all completed compliance projects related to a project`,
        text: `SELECT * from "public".rev_compliance_projects WHERE owner_id=${id} AND end_date is not null`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getNotStartedComplianceProjectByEntityId = async id => {
    const query = {
        name: `Get all compliance projects with no start date`,
        text: `SELECT * from "public".rev_compliance_projects WHERE owner_id=${id} AND start_date is not null`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getOnGoingComplianceProjectByEntityId = async id => {
    const query = {
        name: `Get all ongoing compliance projects related to a project`,
        text: `SELECT * from "public".rev_compliance_projects WHERE owner_id=${id} AND end_date is null AND start_date is not null`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getCompletedComplianceProject = async () => {
    const query = {
        name: `Get all completed compliance projects related to a project`,
        text: `SELECT count(cp.id), cr.status as status,cp.type_id as type_id
        FROM public.rev_compliance_projects cp
        LEFT JOIN public.rev_compliance_records cr
        ON cp.id = cr.compliance_project_id  where cr.status=4 group by cr.status,type_id`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getNotStartedComplianceProject = async () => {
    const query = {
        name: `Get all compliance projects with no start date`,
        text: `SELECT count(cp.id), cr.status as status,cp.type_id as type_id
        FROM public.rev_compliance_projects cp
        LEFT JOIN public.rev_compliance_records cr
        ON cp.id = cr.compliance_project_id  where cr.status=2 group by cr.status,type_id`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getOnGoingComplianceProject = async () => {
    const query = {
        name: `Get all ongoing compliance projects related to a project`,
        text: `SELECT count(cp.id), cr.status as status,cp.type_id as type_id
        FROM public.rev_compliance_projects cp
        LEFT JOIN public.rev_compliance_records cr
        ON cp.id = cr.compliance_project_id  where cr.status in (1, null) group by cr.status,type_id`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getComplianceProjectsCountByEntityId = async id => {
    const query = {
        name: 'Select compliance Projects by entity id',
        text: 'SELECT count(id) from "public".rev_compliance_projects WHERE id=$1',
        values: [ id ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const getComplianceProjectsCount = async () => {
    const query = {
        name: 'Select compliance Projects',
        text: 'SELECT count(id) from "public".rev_compliance_projects',
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}

const getComplianceProjectsCountByMonth = async (fromDate, toDate) => {
    const query = {
        name: 'Select compliance Projects',
        text: `SELECT count(id) from "public".rev_compliance_projects where created_at between '${fromDate}' and '${toDate}'`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}

const addAnswersToProject = async details => {
    const query = {
        name: 'add answers to project',
        text: 'UPDATE "public".rev_compliances_projects SET answer=$1 WHERE id=$2',
        values: [
            details.answer,
            details.id,
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const getCompletedComplianceProjectByMonth = async (fromDate, toDate) => {

    const query = {
        name: `Get all completed compliance projects related to a project`,
        text: `SELECT count(cp.id), cr.status as status,cp.type_id as type_id
        FROM public.rev_compliance_projects cp
        LEFT JOIN public.rev_compliance_records cr
        ON cp.id = cr.compliance_project_id  where cr.status=4 and cr.created_at between '${fromDate}' and '${toDate}' group by cr.status,type_id`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getNotStartedComplianceProjectByMonth = async (fromDate, toDate) => {
    const query = {
        name: `Get all compliance projects with no start date`,
        text: `SELECT count(cp.id), cr.status as status,cp.type_id as type_id
        FROM public.rev_compliance_projects cp
        LEFT JOIN public.rev_compliance_records cr
        ON cp.id = cr.compliance_project_id  where cr.status=2 and cr.created_at between '${fromDate}' and '${toDate}' group by cr.status,type_id`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getOnGoingComplianceProjectByMonth = async (fromDate, toDate) => {
    const query = {
        name: `Get all ongoing compliance projects related to a project`,
        text: `SELECT count(cp.id), cr.status as status,cp.type_id as type_id
        FROM public.rev_compliance_projects cp
        LEFT JOIN public.rev_compliance_records cr
        ON cp.id = cr.compliance_project_id  where cr.status in (1, null) and cr.created_at between '${fromDate}' and '${toDate}' group by cr.status,type_id`,

    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const getComplianceProjectByType = async (type, fromDate, toDate) => {
    const query = {
        name: `Get all Eservices Id`,
        text: `SELECT id, project_name, project_name_ar from "public".rev_compliance_projects WHERE created_at >= '${fromDate}' and created_at < '${toDate}' and type_id = ${type}`,
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
}

const getScore = async (id, fromDate, toDate ) => {
    const query = {
        name: `get score for compliance projects for ${id}`,
        text: `SELECT  COUNT(*) as total_counter, COUNT(*) FILTER (WHERE result=1) AS fully_compline_counter,  COUNT(*) FILTER (WHERE result=2) AS partially_counter
        FROM public.rev_compliance_records where compliance_project_id=${id} and created_at >= '${fromDate}' and created_at < '${toDate}'`
    }

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
}



module.exports = { insertComplianceProject, 
    addAnswersToProject, getComplianceProjectsCount, getComplianceProjectsCountByEntityId,
    getComplianceProjectByEntityId, getCompletedComplianceProjectByEntityId,
    getOnGoingComplianceProjectByEntityId, getNotStartedComplianceProjectByEntityId,
    getCompletedComplianceProject, getNotStartedComplianceProject, getOnGoingComplianceProject,
    getCompletedComplianceProjectByMonth, getNotStartedComplianceProjectByMonth, getOnGoingComplianceProjectByMonth,
    getComplianceProjectsCountByMonth, getComplianceProjectByType, getScore };

