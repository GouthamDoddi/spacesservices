'use strict';

const pool = require('../config/db');


// all project related services will be here

const insertProject = async details => {
    const query = {
        name: 'insert new project',
        text: `INSERT INTO "public".rev_projects(
            project_name, project_name_ar, project_description,
            project_description_ar, sponsor_id, owner_id,
            project_type_id, logo_code, created_by, project_short_name
        ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING *`,
        values: [
            details.name,
            details.nameAr,
            details.description,
            details.descriptionAr,
            details.sponsor,
            details.owner,
            details.projectType,
            details.logo,
            details.createdBy,
            details.projectShortName,
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const updateProject = async details => {
    const query = {
        name: 'Update project details',
        text: `UPDATE "public".rev_projects SET ${details[1]}=$1
        WHERE id=$2`,
        values: [ details[0], details[2] ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
};

const deleteProject = async id => {
    const query = {
        name: 'Delete project row',
        text: 'DELETE * from "public".rev_projects WHERE id=$1',
        values: [ id ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

const selectProject = async id => {
    const query = {
        name: 'Select project',
        text: 'SELECT * from "public".rev_projects WHERE id=$1',
        values: [ id ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.error(error);

        return error;
    }
};

module.exports = { insertProject,
    updateProject,
    deleteProject,
    selectProject, };
