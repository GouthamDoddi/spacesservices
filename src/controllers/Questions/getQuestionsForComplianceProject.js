const { getQuetionsByComplianceProjectType } = require('../../services/QuestionsServices');

// the below function gets questyons for given project type

const fetchQestionsByComplianceProhjectType = async (req, res) => {
    const { type } = req.body;

    const result = await getQuetionsByComplianceProjectType(type);

    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: 'No matching results for this type.',
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: result.rows,
    });
};

module.exports = fetchQestionsByComplianceProhjectType;
