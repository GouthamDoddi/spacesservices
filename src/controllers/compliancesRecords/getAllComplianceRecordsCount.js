const { getAllRows } = require('../../services/servicesTemplate')


const countAllComplianceRecords = async ( req, res ) => {
    const { fromDate, toDate } = req.body;
    const result = await getAllRows('rev_compliance_records', fromDate, toDate)

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: result.rows,
    });
}

module.exports = countAllComplianceRecords;