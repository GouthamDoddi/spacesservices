const { attributecount,complianceissues, complianceIssuesAnalisisWithProjectID, eservicelist, eservicetablelist } = require('../../services/complianceCountServices');

const attributescount = async (req, res) => {
    const  {id, fromDate, toDate}  =  req.body;
    const result = await attributecount(id, fromDate, toDate);
    // console.log("--------", req.body)

    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail,
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: result.rows,
    });
  
};


const complianceIssues = async (req, res) => {
    const  {id, fromDate, toDate}  =  req.body;
    const result = await complianceissues(id, fromDate, toDate);
    // console.log("--------", result)

    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail,
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: result.rows,
    });
  
};

const complianceIssuesAnalisisWithProject = async (req, res) => {
    const { id, fromDate, toDate } = req.body;
    // console.log("==========",req.body )
    const result = await complianceIssuesAnalisisWithProjectID(id, fromDate, toDate);

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: result.rows,
    });
}


const eServicesCompliance = async (req, res) => {
    const  {id, fromDate, toDate}  =  req.body;
    // console.log("==========",req.query )
    const result = await eservicelist(id, fromDate, toDate);

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: result.rows,
    });
}

const eserviceListTable = async (req, res) => {
    const  {id}  =  req.body;
    const result = await eservicetablelist(id);

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: result,
    });
}


module.exports ={ attributescount, complianceIssues, complianceIssuesAnalisisWithProject, eServicesCompliance, eserviceListTable};
