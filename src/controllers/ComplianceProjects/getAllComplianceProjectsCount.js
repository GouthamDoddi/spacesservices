const { getCompletedComplianceProjectByEntityId, getOnGoingComplianceProjectByEntityId,
        getNotStartedComplianceProjectByEntityId, getCompletedComplianceProject,
    getNotStartedComplianceProject, getOnGoingComplianceProject, getComplianceProjectsCountByEntityId,
    getComplianceProjectsCount, getCompletedComplianceProjectByMonth, getNotStartedComplianceProjectByMonth,
     getOnGoingComplianceProjectByMonth, getComplianceProjectsCountByMonth } = require('../../services/ComplianceProjects');




const getAllComplianceProjectsCount = async (req, res) => {
    const { toDate, fromDate } = req.query;


    const allComplianceProjects = await getComplianceProjectsCount(fromDate, toDate)
    const completedComplianceProjects = await  getCompletedComplianceProject(fromDate, toDate)
    const onGoingCompliancePojects = await getOnGoingComplianceProject(fromDate, toDate)
    const notStartedComplianceProjects = await getNotStartedComplianceProject(fromDate, toDate)

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: {
            allComplianceProjects: (allComplianceProjects.rows),
            completeComplianceProjects: (completedComplianceProjects.rows),
            onGoingCompliancePojects: (onGoingCompliancePojects.rows),
            notStartedComplianceProjects: (notStartedComplianceProjects.rows),
        },
    });
}

const getAllComplianceProjectsCountByMonth = async (req, res) => {
    const { toDate, fromDate } = req.params;

    const allComplianceProjects = await getComplianceProjectsCountByMonth(fromDate, toDate)
    const completedComplianceProjects = await  getCompletedComplianceProjectByMonth(fromDate, toDate)
    const onGoingCompliancePojects = await getOnGoingComplianceProjectByMonth(fromDate, toDate)
    const notStartedComplianceProjects = await getNotStartedComplianceProjectByMonth(fromDate, toDate)

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: {
            allComplianceProjects: allComplianceProjects.rows,
            completeComplianceProjects: completedComplianceProjects.rows,
            onGoingCompliancePojects: onGoingCompliancePojects.rows,
            notStartedComplianceProjects: notStartedComplianceProjects.rows,
        },
    });
}

const getAllComplianceProjectsCountByEntityId = async (req, res) => {
    const { entityId } = req.body;

    const allComplianceProjects = await getComplianceProjectsCountByEntityId(parseInt(entityId))
    const completedComplianceProjects = await  getCompletedComplianceProjectByEntityId(parseInt(entityId))
    const onGoingCompliancePojects = await getOnGoingComplianceProjectByEntityId(parseInt(entityId))
    const notStartedComplianceProjects = await getNotStartedComplianceProjectByEntityId(parseInt(entityId))

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: {
            allComplianceProjects: (allComplianceProjects.rows),
            completeComplianceProjects: (completedComplianceProjects.rows),
            onGoingCompliancePojects: (onGoingCompliancePojects.rows),
            notStartedComplianceProjects: (notStartedComplianceProjects.rows),
        },
    });
}

module.exports = { getAllComplianceProjectsCount, getAllComplianceProjectsCountByEntityId,
    getAllComplianceProjectsCountByMonth,  };