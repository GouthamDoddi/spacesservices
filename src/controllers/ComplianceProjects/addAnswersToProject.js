const { addAnswersToProject } = require('../../services/ComplianceProjects');

const updateAnswerForComplianceProjetcs = async (req, res) => {
    const { answers, id } = req.body;

    const details = {
        answers,
        id,
    };

    const result = await addAnswersToProject(details);

    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail,
        });
    }


    return res.json({
        statusCode: 200,
        message: 'Row updated!',
    });
};

module.exports = updateAnswerForComplianceProjetcs;
