const { insertProject } = require('../../services/ProjectServices');

const addComplianceProject = async (req, res) => {
    const { name, description, nameAr,
        descriptionAr, createdBy, consumerType, projectId,
        projectSpoc, startDate, endDate, notes, type } = req.body;


    const projectDetails = {
        name,
        description,
        consumerType,
        nameAr,
        descriptionAr,
        createdBy,
        projectSpoc,
        startDate,
        endDate,
        notes,
        type,
        projectId,
    };

    // calling the service and passing details as arg
    const result = await insertProject(projectDetails);

    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail,
        });
    }

    // returning the added coulumn details so the frontend
    // can save the Id of couloumn if they want to.
    return res.json({
        statusCode: 200,
        message: 'Project Added!',
        data: result.rows,
    });
};

module.exports = addComplianceProject;
