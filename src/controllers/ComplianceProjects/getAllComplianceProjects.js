const { getAllRows } = require('../../services/servicesTemplate')

const getAllComplianceProjects = async (req, res) => {
    const { toDate, fromDate } = req.body;

    const result = await getAllRows('rev_compliance_projects', fromDate, toDate)
    console.log(result)


    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail? result.detail : `${result.rowCount} rows found`
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        noOfProjects: result.rowCount,
        data: result.rows,
    });
};

module.exports = getAllComplianceProjects;