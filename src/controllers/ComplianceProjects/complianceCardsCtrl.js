const {  getRevComplianceProject,
    getComplianceRecordsStatus,
    getComplianceRecordsMandate,
    getComplianceIssuesAnalysis,
    getComplianceSection3,
    getExceptionCases,
    getRegisteredStatusCountApps,
    getCompletedComplianceProjectAnalysis,
    eservice_cards_service
} = require('../../services/ComplianceDashService');

const selectComplianceProject = async (req, res) => {
    const { id, fromDate, toDate } = req.query;

    console.log("id=========>",id);

    const results = await getRevComplianceProject(id, fromDate, toDate);
    if(results.rows.length==0){

        return res.json({
            statusCode: 400,
            message: "no data found",
            data: [],
        });

    }


try{
    const run = async _ => {
        console.log("Start",results.rows.length)
        var arr=[];
        await results.rows.forEach(async (result) => {
            console.log("result========>");
            var result1 = await getComplianceRecordsMandate(result.id, fromDate, toDate);
            var result2 = await getComplianceRecordsStatus(result.id, fromDate, toDate);
            result.mandate=result1.rows;
            result.status=result2.rows;
            arr.push(result);
            if(arr.length==results.rows.length){
                        return res.json({
                             statusCode: 200,
                             message: 'Sucesses',
                             data: arr,
                 });            
             }
        });
      };
      run();
    }catch(err){
            return res.json({
                statusCode: 400,
                message: err,
            });
    }


    
};



const getIssuesAnalysis = async (req, res) => {
    const { id, fromDate, toDate } = req.query;

    const result = await getComplianceIssuesAnalysis(id, fromDate, toDate);
    
    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail? result.detail : `${result.rowCount} rows found`,
            data: [],
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        noOfProjects: result.rowCount,
        data: result.rows,
    });

};

const getSection3 = async (req, res) => {
    const { id, fromDate, toDate } = req.query;

    const result = await getComplianceSection3(id, fromDate, toDate);
    
    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail? result.detail : `${result.rowCount} rows found`,
            data: [],
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        noOfProjects: result.rowCount,
        data: result.rows,
    });

};

const exceptionCases = async (req, res) => {
    const { id, fromDate, toDate } = req.query;

    const result = await getExceptionCases(id, fromDate, toDate);
    
    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail? result.detail : `${result.rowCount} rows found`,
            data: [],
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        noOfProjects: result.rowCount,
        data: result.rows,
    });

};


const getRegisteredStatusCountCtrlApps = async (req, res) => {
    const { id, fromDate, toDate } = req.query;

    const result = await getRegisteredStatusCountApps(id, fromDate, toDate);
    
    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail? result.detail : `${result.rowCount} rows found`,
            data: [],
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        noOfProjects: result.rowCount,
        data: result.rows,
    });
}

const getCompletedComplianceProjectCtrlAnalysis = async (req, res) => {
    const { id, fromDate, toDate } = req.query;

    const result = await getCompletedComplianceProjectAnalysis(id,fromDate, toDate);
    
    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail? result.detail : `${result.rowCount} rows found`,
            data: [],
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        noOfProjects: result.rowCount,
        data: result.rows,
    });
}

const eservice_cards = async (req, res) => {
    const { id, fromDate, toDate } = req.query;

    const result = await eservice_cards_service(id,fromDate, toDate);
    
    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail? result.detail : `${result.rowCount} rows found`,
            data: [],
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        noOfProjects: result.rowCount,
        data: result.rows,
    });
}




module.exports = {selectComplianceProject,getIssuesAnalysis,getSection3,exceptionCases,
    getRegisteredStatusCountCtrlApps,
    getCompletedComplianceProjectCtrlAnalysis,
    eservice_cards
};