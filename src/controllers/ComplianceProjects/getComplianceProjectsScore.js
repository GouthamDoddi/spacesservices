const { getScore, getComplianceProjectByType } = require('../../services/ComplianceProjects')

const calScore = rows  => parseInt(((rows.fully_compline_counter + (rows.partially_counter*0.5))/rows.total_counter) * 100)


const getEservicesScore = async ( req, res ) => {
    const {fromDate, toDate} = req.query;

    const results = await getComplianceProjectByType(3, fromDate, toDate);


    try{
        const run = async _ => {
            console.log("Start",results.rows.length)
            var arr=[];
            await results.rows.forEach(async (result) => {
                console.log("result========>");
                const result1 = await getScore(result.id, fromDate, toDate)
                result.score=calScore(result1.rows[0]);
                arr.push(result);
                if(arr.length==results.rows.length){
                            return res.json({
                                 statusCode: 200,
                                 message: 'Sucesses',
                                 data: arr,
                     });            
                 }
            });
          };
          run();
        }catch(err){
                return res.json({
                    statusCode: 400,
                    message: err,
                });
        }
}

const getPortalsScore = async ( req, res ) => {
    const {fromDate, toDate} = req.query;

    const results = await getComplianceProjectByType(1, fromDate, toDate);


    try{
        const run = async _ => {
            console.log("Start",results.rows.length)
            var arr=[];
            await results.rows.forEach(async (result) => {
                console.log("result========>");
                const result1 = await getScore(result.id, fromDate, toDate)
                result.score=calScore(result1.rows[0]);
                arr.push(result);
                if(arr.length==results.rows.length){
                            return res.json({
                                 statusCode: 200,
                                 message: 'Sucesses',
                                 data: arr,
                     });            
                 }
            });
          };
          run();
        }catch(err){
                return res.json({
                    statusCode: 400,
                    message: err,
                });
        }
}

const getMobileAppsScore = async ( req, res ) => {
    const {fromDate, toDate} = req.query;

    const results = await getComplianceProjectByType(2, fromDate, toDate);


    try{
        const run = async _ => {
            console.log("Start",results.rows.length)
            var arr=[];
            await results.rows.forEach(async (result) => {
                console.log("result========>");
                const result1 = await getScore(result.id, fromDate, toDate)
                result.score=calScore(result1.rows[0]);
                arr.push(result);
                if(arr.length==results.rows.length){
                            return res.json({
                                 statusCode: 200,
                                 message: 'Sucesses',
                                 data: arr,
                     });            
                 }
            });
          };
          run();
        }catch(err){
                return res.json({
                    statusCode: 400,
                    message: err,
                });
        }
}

module.exports = { getEservicesScore, getPortalsScore, getMobileAppsScore };
