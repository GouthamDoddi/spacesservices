const { getComplianceProject } = require('../../services/ComplianceProjects');

const selectComplianceProject = async (req, res) => {
    const { id } = req.body;

    const result = await getComplianceProject(id);

    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail,
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: result.rows,
    });
};

module.exports = selectComplianceProject;
