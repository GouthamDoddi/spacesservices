const { deleteProject } = require('../../services/ProjectServices');

const removeProject = async (req, res) => {
    const { id } = req.body;

    const result = await deleteProject(id);

    if (!result.rowCount) {
        return res.json({
            status: 400,
            message: 'failed',
            data: result.details,
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Deleted!',
    });
};

module.exports = removeProject;
