const { updateProject } = require('../../services/ProjectServices');

const updateProjectDetails = async (req, res) => {
    const { name, projectShortName, description, sponsor,
        owner, projectType, nameAr, descriptionAr, logo, createdBy, id } = req.body;

    // queryDetails only selects only the data that is present in the req.body
    // LIMITATION only one column can be updated at once.
    const queryDetails = name
        ? [ name, 'project_name', id ]
        : nameAr
            ? [ nameAr, 'project_name_ar', id ]
            : description
                ? [ description, 'project_description', id ]
                : descriptionAr
                    ? [ descriptionAr, 'project_description_ar', id ]
                    : projectShortName
                        ? [ projectShortName, 'project_short_name', id ]
                        : sponsor
                            ? [ sponsor, 'sponsor_id', id ]
                            : owner
                                ? [ owner, 'owner_id', id ]
                                : projectType
                                    ? [ projectType, 'project_type_id', id ]
                                    : logo
                                        ? [ logo, 'logo_code', id ]
                                        : createdBy
                                            ? [ createdBy, 'created_by', id ]
                                            : null;

    if (queryDetails === null) {
        return res.json({
            statusCode: 400,
            message: 'Please check req.body and send correct parameters',
        });
    }

    const result = await updateProject(queryDetails);

    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail,
        });
    }


    return res.json({
        statusCode: 200,
        message: 'Row updated!',
    });
};


module.exports = updateProjectDetails;
