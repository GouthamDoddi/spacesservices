const { insertProject } = require('../../services/ProjectServices');

const addProject = async (req, res) => {
    const { name, projectShortName, description, sponsor,
        owner, projectType, nameAr, descriptionAr, logo, createdBy } = req.body;


    const projectDetails = {
        name,
        projectShortName,
        description,
        sponsor,
        owner,
        projectType,
        logo,
        nameAr,
        descriptionAr,
        createdBy,
    };

    // calling the service and passing details as arg
    const result = await insertProject(projectDetails);

    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail,
        });
    }

    // returning the added coulumn details so the frontend
    // can save the Id of couloumn if they want to.
    return res.json({
        statusCode: 200,
        message: 'Project Added!',
        data: result.rows,
    });
};

module.exports = addProject;
