const { selectProject } = require('../../services/ProjectServices');

const getProject = async (req, res) => {
    const { id } = req.body;

    const result = await selectProject(id);

    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail,
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: result.rows,
    });
};

module.exports = getProject;
