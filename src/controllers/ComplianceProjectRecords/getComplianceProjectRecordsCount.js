const { getAllComplianceProjectRecordsCount } = require('../../services/ComplianceProjectRecords');


const countAllComplianceProjectRecords = async ( req, res ) => {
    const result = await getAllComplianceProjectRecordsCount();

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: result,
    });
}

module.exports = countAllComplianceProjectRecords;