const { complianceRecordsAnalisis } = require('../../services/ComplianceProjectRecords');

const complianceRecordsAnalysis = async (req, res) => {
    const { toDate, fromDate } = req.query;

    const result = await complianceRecordsAnalisis(fromDate, toDate);

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: result.rows,
    });
}

module.exports = complianceRecordsAnalysis;