const { getAllRows } = require('../../services/servicesTemplate')

const getAllPolicies = async ( req, res ) => {
    const { toDate, fromDate } = req.body;

    const result = await getAllRows('policies', fromDate, toDate)

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: {
            noOfPolicies: result.rowCount,
            data: result.rows,
        }
    });
}

module.exports = getAllPolicies;
