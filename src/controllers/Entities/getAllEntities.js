const { getAllRows } = require('../../services/servicesTemplate');
const { getCompletedComplianceProjectAnalysis } = require('../../services/ComplianceDashService');

const getEntities = async (req, res) => {
    const result = await getAllRows('entities')
    console.log(result)


    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail? result.detail : `${result.rowCount} rows found`
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        noOfProjects: result.rowCount,
        data: result.rows,
    });
};

const getEntityCards = async (req, res) => {
    const { toDate, fromDate } = req.query;
    console.log(req.body)

    const results = await getAllRows('entities', fromDate, toDate);

    // console.log(results)
    if(!results.rows.length){

        return res.json({
            statusCode: 400,
            message: "no data found",
            data: [],
        });

    }


// try{
//     const run = async _ => {
//         console.log("Start",results.rows.length)
//         var arr=[];
//         await results.rows.forEach(async (result) => {
//             console.log("result========>");

//             var result1 = await getCompletedComplianceProjectAnalysis(result.id, fromDate, toDate)
//             result.complianceProjects=result1.rows;
//             arr.push(result);
//             if(arr.length==results.rows.length){
//                         return res.json({
//                              statusCode: 200,
//                              message: 'Sucesses',
//                              data: arr,
//                  });            
//              }
//         });
//       };
//       run();
//     }catch(err){
//             return res.json({
//                 statusCode: 400,
//                 message: err,
//             });
//     }


    
};

module.exports = getEntityCards;