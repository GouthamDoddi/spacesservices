const { getAllRows } = require('../../services/servicesTemplate');
const { getEntityScore } = require('../../services/EntitesServices');

const calScore = rows  => parseInt(((rows.fully_compline_counter + (rows.partially_counter*0.5))/rows.total_counter) * 100)

const getEntityPerformance  = async (req, res) => {
    const { toDate, fromDate } = req.query;

    const results = await getAllRows('entities', fromDate, toDate);

    // console.log(results)
    if(!results.rows.length){

        return res.json({
            statusCode: 400,
            message: "no data found",
            data: [],
        });

    }

    try{
        const run = async _ => {
            console.log("Start",results.rows.length)
            var arr=[];
            await results.rows.forEach(async (result) => {
                console.log("result========>");
    
                var result1 = await getEntityScore(result.id, fromDate, toDate)
                result.complianceProjects=calScore(result1.rows[0]);
                arr.push(result);
                if(arr.length==results.rows.length){
                            return res.json({
                                 statusCode: 200,
                                 message: 'Sucesses',
                                 data: arr,
                     });            
                 }
            });
          };
          run();
        }catch(err){
                return res.json({
                    statusCode: 400,
                    message: err,
                    data: [],
                });
        }
} 

module.exports = getEntityPerformance;