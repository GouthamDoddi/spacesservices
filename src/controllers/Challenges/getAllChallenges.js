const { getAllRows } = require('../../services/servicesTemplate')

const getChallenges = async (req, res) => {
    const { fromDate, toDate } = req.query;
    const result = await getAllRows('challenges', fromDate, toDate)
    console.log(result)

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        data: {
            noOfProjects: result.rowCount,
            data: result.rows,
        }
    });
};

module.exports = getChallenges;