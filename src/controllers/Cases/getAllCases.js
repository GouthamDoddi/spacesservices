const { getAllRows } = require('../../services/servicesTemplate')

const getCases = async (req, res) => {
    const { fromDate, toDate } = req.query;

    const result = await getAllRows('cases', fromDate, toDate)
    console.log(result)


    if (!result.rowCount) {
        return res.json({
            statusCode: 400,
            message: result.detail
            ? result.detail 
            : `${result.rowCount} rows found`,
            noOfProjects: 0,
            data: [],
        });
    }

    return res.json({
        statusCode: 200,
        message: 'Sucesses',
        noOfProjects: result.rowCount,
        data: result.rows,
    });
};

module.exports = getCases;