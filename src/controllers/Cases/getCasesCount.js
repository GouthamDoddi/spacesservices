const { getCasesCount }  = require('../../services/CasesServices')

const getCasesCountByMonth = async (req, res) => {
    const { month } = req.body;

    const monthData = `${month} months`

    const result = await getCasesCount(monthData)

    return res.json({
        statusCode: 200,
        data: result.rows,
    });
}

module.exports = { getCasesCountByMonth }