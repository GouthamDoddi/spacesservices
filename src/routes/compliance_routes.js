'use strict';

const express = require('express');
// const multer = require('multer');
const router = express.Router();
// const formDataHandler = multer();

const complianceCtrl = require('../controllers/ComplianceProjects/complianceCardsCtrl');

router.get('/compliance_cards', complianceCtrl.selectComplianceProject);
router.get('/getIssuesAnalysis', complianceCtrl.getIssuesAnalysis);
router.get('/section_3', complianceCtrl.getSection3);
router.get('/exception_cases', complianceCtrl.exceptionCases);
router.get('/registered_status_count_apps', complianceCtrl.getRegisteredStatusCountCtrlApps);
router.get('/compliance_project_analysis', complianceCtrl.getCompletedComplianceProjectCtrlAnalysis);


router.get('/eservice_cards', complianceCtrl.eservice_cards);



module.exports = router;
