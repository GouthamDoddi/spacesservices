/* eslint-disable new-cap */
'use strict';

const express = require('express');
// const multer = require('multer');
const router = express.Router();
// const formDataHandler = multer();


//projects
const addProject = require('../controllers/Projects/addProject');
const removeProject = require('../controllers/Projects/deleteProjects');
const updateProjectDetails = require('../controllers/Projects/editProject');
const getProject = require('../controllers/Projects/selectProject');
const getProjects = require('../controllers/Projects/getAllProjects');

//complianceProjects
const updateAnswerForComplianceProjetcs = require('../controllers/ComplianceProjects/addAnswersToProject');
const addComplianceProject = require('../controllers/ComplianceProjects/addComplianceProjects');
const selectComplianceProject = require('../controllers/ComplianceProjects/getComplinaceProjects');
const getAllComplianceProjects = require('../controllers/ComplianceProjects/getAllComplianceProjects');
const { getAllComplianceProjectsCountByEntityId, 
    getAllComplianceProjectsCount, getAllComplianceProjectsCountByMonth } = require('../controllers/ComplianceProjects/getAllComplianceProjectsCount');
const { getEservicesScore, getMobileAppsScore, getPortalsScore  } = require('../controllers/ComplianceProjects/getComplianceProjectsScore');



//complianceCount
const complianceAttributes = require('../controllers/compliancesRecords/complianceCount');

//questions
const getQuestionsForComplianceProject = require('../controllers/Questions/getQuestionsForComplianceProject');

//entities
const getEntityCards = require('../controllers/Entities/getAllEntities');
const getEntityPerformance = require('../controllers/Entities/getEntityPerformance')

//challenges
const getChallenges = require('../controllers/Challenges/getAllChallenges');

//cases
const getCases = require('../controllers/Cases/getAllCases');
const { getCasesCountByMonth } = require('../controllers/Cases/getCasesCount');

//policies
const getAllPolicies = require('../controllers/Policies/getAllPolicies');

//complianceProjectRecords
const getAllComplianceProjectRecordsCount = require('../controllers/ComplianceProjectRecords/getComplianceProjectRecordsCount')
const complianceRecordsAnalysis = require('../controllers/ComplianceProjectRecords/complianceProjectRecordsAnalysis')

//compliancerecords
const countAllComplianceRecords  = require('../controllers/compliancesRecords/getAllComplianceRecordsCount')

// // index
// const pug = require('pug');

// const index = pug.compileFile('index.pug')

// router.get('', (req, res) => res.render(index))

router.post('/add_project', addProject);
router.post('/delete_project', removeProject);
router.post('/edit_project', updateProjectDetails);
router.post('/select_project', getProject);
router.get('/get_all_projects', getProjects);

//complianceProjects
router.post('/add_complianceProject', addComplianceProject);
router.post('/update_answer_for_compliance_projetcs', updateAnswerForComplianceProjetcs);
router.post('/getComplianceProject', selectComplianceProject);
router.post('/getAllComplianceProjects', getAllComplianceProjects);
router.post('/getAllComplianceProjectsCount_by_entityId', getAllComplianceProjectsCountByEntityId);
router.get('/get_all_compliance_projects_count', getAllComplianceProjectsCount)
router.get('/get_all_compliance_projects_count_by_month', getAllComplianceProjectsCountByMonth);
router.get('/get_eservices_score', getEservicesScore);
router.get('/get_portals_score', getPortalsScore);
router.get('/get_mobile_app_score', getMobileAppsScore);


//questions
router.get('/getQuestionsForComplianceProject', getQuestionsForComplianceProject);

//entities
router.get('/getEntities', getEntityCards);
router.get('/get_entity_performance', getEntityPerformance)

//challenges
router.get('/getChallenges', getChallenges);

//cases
router.get('/getCases', getCases);
router.post('/get_cases_count_by_month', getCasesCountByMonth)

//complianceAttributes
router.get('/attributes_count', complianceAttributes.attributescount)
router.get('/compliance_ssues', complianceAttributes.complianceIssues)
router.get('/compliance_analysis_issues', complianceAttributes.complianceIssuesAnalisisWithProject)
router.get('/eServices_list', complianceAttributes.eServicesCompliance)
router.get('/eServices_list_table', complianceAttributes.eserviceListTable)
//policies
router.get('/getPolicies', getAllPolicies);

//complianceProjectRecords
router.get('/get_compliance_project_records_count', getAllComplianceProjectRecordsCount);
router.get('/compliance_issues_analysis', complianceRecordsAnalysis);

//compliance records
router.get('/get_issues_count', countAllComplianceRecords);


module.exports = router;
